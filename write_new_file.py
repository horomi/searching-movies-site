import csv


# new_file = open('movies_db_del/shot_principal', "w")
# with open('movies_db_del/principal.tsv') as fd:
#     rd = csv.reader(fd, delimiter="\t", quotechar='"')
#     for row in rd:
#         if row[3] == "self" or row[3] == "actor" or row[3] == "actress":
#             modificated_row = str(row[0]) + " " + str(row[2]) + "\n"
#             new_file.write(modificated_row)
# new_file.close()


# f = open('movies_db_del/shot_names', 'w+')
# f.seek(0)
# f.close()


# new_file = open('movies_db_del/shot_names', "w")
# with open('movies_db_del/names.tsv') as fd:
#     rd = csv.reader(fd, delimiter="\t", quotechar='"')
#     for row in rd:
#         modificated_row = str(row[0]) + " " + str(row[1]) + "\n"
#         new_file.write(modificated_row)
# new_file.close()


# new_file = open('movies_db_del/shot_titles', "w")
# with open('movies_db_del/titles.tsv') as fd:
#     rd = csv.reader(fd, delimiter="\t", quotechar='"')
#     for row in rd:
#         if row[1] == "tvEpisode" or row[1] == "tvSeries":
#             pass
#         else:
#             modificated_row = str(row[0]) + " " + str(row[2]) + "\n"
#             new_file.write(modificated_row)
# new_file.close()

#
# new_file = open('movie_db/titles', "w")
# with open('movies_db_del/titles.tsv') as fd:
#     rd = csv.reader(fd, delimiter="\t", quotechar='"')
#     for row in rd:
#         if row[1] == "movie" or row[1] == "tvMovie":
#             modificated_row = str(row[0]) + " " + str(row[2]) + "\n"
#             new_file.write(modificated_row)
# new_file.close()


def create_dict_movie_name_id(filename):
    """The function reads file and create a new dictionary,
    where key - id-title, value - name of movie"""
    fin_id_title = open(filename)
    id_tt_dict = {}
    for line in fin_id_title:
        title_id, name_movie = line.split(" ", 1)
        if title_id[2:].isdigit() is True:
            id_tt_dict[int(title_id[2:])] = 0
    return id_tt_dict


# dict_tt = create_dict_movie_name_id('movie_db/titles')
#
# new_file = open('movie_db/principal', "w")
# with open('movies_db_del/principal.tsv') as fd:
#     rd = csv.reader(fd, delimiter="\t", quotechar='"')
#     for row in rd:
#         if row[0][2:].isdigit() is True:
#             # print(row[3], row[0], int(row[0][2:]))
#             if (row[3] == "self" or row[3] == "actor" or row[3] == "actress") and (int(row[0][2:]) in dict_tt):
#                 modificated_row = str(row[0]) + " " + str(row[2]) + "\n"
#                 new_file.write(modificated_row)
# new_file.close()

def create_dict_id_actor_id_title(filename):
    """The function reads file and create a new dictionary, where
    key - id-name of an actor, value - list of id-titles of this actor"""
    fin_actor_title = open(filename)
    id_nm_dict = {}
    for line in fin_actor_title:
        title_id, name_id = line.split()
        name_id = int(name_id[2:])
        id_nm_dict[name_id] = 0
    return id_nm_dict

# dict_nm = create_dict_id_actor_id_title('movie_db/principal')
#
#
# new_file = open('movie_db/names', "w")
# with open('movies_db_del/names.tsv') as fd:
#     rd = csv.reader(fd, delimiter="\t", quotechar='"')
#     for row in rd:
#         if (row[0][2:].isdigit() is True) and (int(row[0][2:]) in dict_nm):
#             modificated_row = str(row[0]) + " " + str(row[1]) + "\n"
#             new_file.write(modificated_row)
# new_file.close()



