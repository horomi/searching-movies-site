from flask import Flask, render_template, request
from search_movie import search_movies_by_actor_names
from create_dict import create_dict_movie_name_id, create_dict_actor_name_id, create_dict_id_actor_id_title

app = Flask(__name__)


@app.route('/')
@app.route('/home')
def index():
    return render_template("index.html")


@app.route('/about')
def about():
    return render_template("about.html")


@app.route('/', methods=['POST', 'GET'])
@app.route('/search_result', methods=['POST', 'GET'])
def search_result():
    actor_name_1 = request.form['search1']
    actor_name_2 = request.form['search2']
    if actor_name_2 == "":
        actor_name_2 = actor_name_1
    result = search_movies_by_actor_names(actor_name_1, actor_name_2, id_actor_name, id_nm_id_tt, id_movie_name)
    return render_template("search_result.html",
                           result=result,
                           name1=actor_name_1.title(),
                           name2=actor_name_2.title())


if __name__ == "__main__":
    id_actor_name = create_dict_actor_name_id('movie_db/names')
    id_nm_id_tt = create_dict_id_actor_id_title('movie_db/principal')
    id_movie_name = create_dict_movie_name_id('movie_db/titles')
    app.run(debug=True)
