import bisect


def find_word_sorted(words: list, word: str):
    """The function takes a sorted list and the desired value and
    returns the index of this value in the list, or None if it is not there
    """
    # by bisect module
    i = bisect.bisect_left(words, word)
    if i < len(words) and word == words[i]:
        return i
    else:
        return None


def search_movies_by_actor_names(name1: str, name2: str, id_actor_name, id_nm_id_tt, id_movie_name) -> list or str:
    """The function takes the names of 2 actors and 3 dictionaries with data about the id names of actors
    and titles of movies and searches given names in dictionaries. """
    name1 = name1.lower()
    name2 = name2.lower()
    list_common_movies = []
    if (name1 in id_actor_name) and (name2 in id_actor_name):
        id_actor1 = id_actor_name[name1]  # getting id first actor
        id_actor2 = id_actor_name[name2]  # getting id second actor
        if (id_actor1 in id_nm_id_tt) and (id_actor2 in id_nm_id_tt):
            # getting lists with id titles of movies two actors
            list_of_id_actor_titles_1 = id_nm_id_tt[id_actor1]
            list_of_id_actor_titles_2 = id_nm_id_tt[id_actor2]
            for id_title in list_of_id_actor_titles_1:
                # take every movie from the first actor's movie list and search that in the second actor's movie list
                # searching by bisection algorithm
                index_common_title = find_word_sorted(list_of_id_actor_titles_2, id_title)
                if index_common_title is not None:
                    id_of_common_title = list_of_id_actor_titles_2[index_common_title]
                    if id_of_common_title in id_movie_name:
                        name_of_common_title = id_movie_name[id_of_common_title]
                        list_common_movies.append(name_of_common_title)
            return list_common_movies
        else:
            return f"{name2.title()} not found"
    else:
        return f"{name1.title()} not found"
