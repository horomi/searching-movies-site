def create_dict_actor_name_id(filename) -> dict:
    """The function reads file and create a new dictionary,
    where key - name of an actor, value - id of an actor"""
    fin_name = open(filename)
    actors_name_and_id_dict = {}
    same_names_dict = {}
    for line in fin_name:
        actor_id, name = line.split(" ", 1)
        if actor_id[2:].isdigit() is True:
            if name[:-1].lower() in actors_name_and_id_dict and int(actor_id[2:]) > actors_name_and_id_dict[name[:-1].lower()]:
                pass
            else:
                actors_name_and_id_dict[name[:-1].lower()] = int(actor_id[2:])
    return actors_name_and_id_dict


def create_dict_id_actor_id_title(filename) -> dict:
    """The function reads file and create a new dictionary, where
    key - id-name of an actor, value - list of id-titles of this actor"""
    fin_actor_title = open(filename)
    id_actor_and_id_titles_dict = {}
    for line in fin_actor_title:
        title_id, name_id = line.split()
        title_id = int(title_id[2:])
        name_id = int(name_id[2:])
        if name_id in id_actor_and_id_titles_dict:
            id_actor_and_id_titles_dict[name_id].append(title_id)
        else:
            id_actor_and_id_titles_dict[name_id] = [title_id]
    for key in id_actor_and_id_titles_dict:
        id_actor_and_id_titles_dict[key].sort()
    return id_actor_and_id_titles_dict


def create_dict_movie_name_id(filename) -> dict:
    """The function reads file and create a new dictionary,
    where key - id-title, value - name of movie"""
    fin_id_title = open(filename)
    id_and_titles_name_dict = {}
    for line in fin_id_title:
        title_id, name_movie = line.split(" ", 1)
        if title_id[2:].isdigit() is True:
            id_and_titles_name_dict[int(title_id[2:])] = name_movie[:-1]
    return id_and_titles_name_dict
